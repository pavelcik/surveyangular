import { Injectable } from '@angular/core';
import { Project } from './project';
import  { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";
import { MessageService} from "./message.service";
import { HttpClient, HttpHeaders} from "@angular/common/http";
import { catchError, map, tap} from 'rxjs/operators'
import { Survey } from './survey';
import {SurveyService} from './survey.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable()
export class ProjectService {

  private userUrl = 'http://localhost:8080/projects';



  constructor(private http: HttpClient,
              private messageService: MessageService,
            private surveyService: SurveyService) { }

  getProjects(): Observable<Project[]> {
    this.messageService.add('Project list loded');
    return this.http.get<Project[]>(this.userUrl)
      .pipe(
        tap(projects => this.log(`fetched projects`)),
        catchError(this.handleError('getProjects', []))
      );
  }

  getProject(id: number): Observable<Project> {
    const url = `${this.userUrl}/${id}`;
    return this.http.get<Project>(url).pipe(
      tap(_ => this.log(`fetched project id=${id}`)),
      catchError(this.handleError<Project>(`getProject id=${id}`))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead

      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    this.messageService.add('ProjectService: ' + message);
  }
  updateProject(project: Project | number): Observable<any> {
    const id = typeof project === 'number' ? project : project.id;
    const url = `${this.userUrl}/${id}`;
    return this.http.put(url,project,httpOptions)
      .pipe(tap(_=>this.log('updated project id=${project.id}')),
        catchError(this.handleError<any>('updateProject')));
  }
  addProject (project: Project): Observable<Project> {

    return this.http.post<Project>(this.userUrl, project, httpOptions).pipe(
      tap((project: Project) => this.log(`added project w/ id=${[project.id]}`)),
      catchError(this.handleError<Project>('addUser'))
    );
  }

  deleteProject (project: Project | number): Observable<Project> {
    const id = typeof project === 'number' ? project : project.id;
    const url = `${this.userUrl}/${id}`;

    return this.http.delete<Project>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted project id=${id}`)),
      catchError(this.handleError<Project>('deleteProject'))
    );
  }
  
  sendSurvey(survey: Survey) {
    this.surveyService.addSurvey(survey);
  }
}
