import {Component, Input, OnInit} from '@angular/core';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";
import {UserGroupService} from "../user-group.service";
import {UserGroup} from "../userGroup";

@Component({
  selector: 'app-user-group',
  templateUrl: './user-group.component.html',
  styleUrls: ['./user-group.component.css']
})
export class UserGroupComponent implements OnInit {
  userGroups: UserGroup[];
  @Input() userGroup: UserGroup;
    order: string = 'name';
  reverse: boolean = false;

  constructor(private userGroupService: UserGroupService,
              private route: ActivatedRoute,
              private modalService: NgbModal,
              private location: Location,) {
  }

  ngOnInit() {
    this.getUserGroups();
  }

  getUserGroups(): void {
    this.userGroupService.getUserGroups()
      .subscribe(userGroups => this.userGroups = userGroups);
  }

  add(groupName:string, usersInGroup:number) {
    if (!groupName || !usersInGroup) {
      return;
    }
    this.userGroupService.addUserGroup({groupName, usersInGroup} as UserGroup)
      .subscribe(userGroup => {
        this.userGroups.push(userGroup);
      });
  }

  delete(userGroup: UserGroup): void {
    this.userGroups = this.userGroups.filter(h => h !== userGroup);
    this.userGroupService.deleteUserGroup(userGroup).subscribe();
  }

  open(userGroup, modal) {
    this.userGroup = userGroup;
    this.modalService.open(modal);
  }

  openAdd(modal) {
    this.modalService.open(modal);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.userGroupService.updateUserGroup(this.userGroup)
      .subscribe(() => this.goBack());
  }
  
    setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }
}
