import {Component, Input, OnInit} from '@angular/core';
import { User } from '../user';
import { UserService} from "../user.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Location} from "@angular/common";
import {ActivatedRoute} from "@angular/router";
import {FormBuilder,FormGroup,Validators} from "@angular/forms";
import {UserGroup} from "../userGroup";


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  users: User[];
  user: User;
  order: string = 'name';
  reverse: boolean = false;
  rForm: FormGroup;
  name: string;
  titleAlert: string = 'This field is required';
  surname: string;
  emailAddress: string;
  userGroup: string;
  userGroups: UserGroup[];
  myForm: FormGroup;


  constructor(private userService: UserService,
              private route: ActivatedRoute,
              private modalService: NgbModal,
              private location: Location,
              private fb: FormBuilder) {
    this.rForm = fb.group({
      'name': [null, Validators.required],
      'surname': [null, Validators.required],
      'emailAddress': [null, Validators.compose([Validators.email])]
    });
  }

  ngOnInit() {
    this.getUsers();
  }

  getUsers(): void {
    this.userService.getUsers()
      .subscribe(users => this.users = users);
  }

  add(name: string, surname: string, emailAddress: string) {
    if (!name || !surname || !emailAddress) {
      return;
    }
    this.userService.addUser({name, surname, emailAddress} as User)
      .subscribe(user => {
        this.users.push(user);
      });
  }

  addForm(user: User) {
    this.name = user.name;
    this.surname = user.surname;
    this.emailAddress = user.emailAddress;
    console.log(user);
    this.userService.addUser(user).subscribe(user=>{
      this.users.push(user);
    });
  }

  delete(user: User): void {
    this.users = this.users.filter(h => h !== user);
    this.userService.deleteUser(user).subscribe();
  }

  open(user, modal) {
    this.user = user;
    this.userService.getUser(user.id);
    this.modalService.open(modal);
  }

  openAdd(modal) {
    this.modalService.open(modal);
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.userService.updateUser(this.user).subscribe();
  }

  setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }


}


