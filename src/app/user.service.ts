import { Injectable } from '@angular/core';
import { User } from './user';
import { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";
import { MessageService} from "./message.service";
import { HttpClient, HttpHeaders} from "@angular/common/http";
import {Http, Response,RequestOptions,Headers} from '@angular/http';

import { catchError, map, tap} from 'rxjs/operators'
import 'rxjs/add/operator/map';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable()
export class UserService {

  private userUrl = 'http://localhost:8080/users';
  users: User[];


  constructor(private http: HttpClient,
              private messageService: MessageService,
              private oldHttp: Http) { }

   getUsers(): Observable<User[]> {
    this.messageService.add('User list loded');
    return this.http.get<User[]>(this.userUrl)
      .pipe(
        tap(users => this.log(`fetched users`)),
        catchError(this.handleError('getUsers', []))
      );
  }

  getUser(id: number): Observable<User> {
    const url = `${this.userUrl}/${id}`;
    return this.http.get<User>(url).pipe(
      tap(_ => this.log(`fetched user id=${id}`)),
      catchError(this.handleError<User>(`getHero id=${id}`))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead

      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    this.messageService.add('UserService: ' + message);
  }
  updateUser(user: User): Observable<User> {
    console.log(user.name);
   return this.http
    .put<User>(this.userUrl,user, httpOptions);
  }
  addUser (user: User):Observable<User> {
    return this.http.post<User>(this.userUrl, user, httpOptions );
  }

  deleteUser (user: User | number): Observable<User> {
    const id = typeof user === 'number' ? user : user.id;
    const url = `${this.userUrl}/${id}`;

    return this.http.delete<User>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted user id=${id}`)),
      catchError(this.handleError<User>('deleteUser'))
    );
  }

  }

