import { NgModule } from '@angular/core';
import { RouterModule,Routes} from "@angular/router";
import { UserComponent } from "./user/user.component";
import {MainPageComponent} from "./main-page/main-page.component";
import {ProjectComponent} from "./project/project.component";
import { SurveyComponent } from './survey/survey.component';
import {UserGroupComponent} from "./user-group/user-group.component";


const routes: Routes = [
  {path: 'users', component: UserComponent},
  {path: 'mainPage', component: MainPageComponent},
  {path: '', redirectTo: '/mainPage', pathMatch: 'full'},
  {path: 'projects', component: ProjectComponent},
  {path: 'userGroups', component: UserGroupComponent},
  {path: 'projects/addSurvey', component: SurveyComponent}
];
@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forRoot(routes)]
})
export class AppRoutingModule { }
