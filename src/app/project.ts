export class Project {
  id: number;
  projectName: string;
  startDate: Date;
  endDate: Date;
  userGroup: any;
}

