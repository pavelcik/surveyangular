import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import  { FormsModule,ReactiveFormsModule } from '@angular/forms';
import  { UserService} from "./user.service";
import { ProjectService} from "./project.service";
import { AppComponent } from './app.component';
import { UserComponent } from './user/user.component';
import { UserGroupComponent } from './user-group/user-group.component';
import { ProjectComponent } from './project/project.component';
import { UserDetailComponent } from './user-detail/user-detail.component';
import { ProjectDetailComponent } from './project-detail/project-detail.component';
import { UserGroupDetailComponent } from './user-group-detail/user-group-detail.component';
import { MessagesComponent } from './messages/messages.component';
import { MessageService } from './message.service';
import { AppRoutingModule } from './/app-routing.module';
import { MainPageComponent } from './main-page/main-page.component';
import { HttpClientModule } from "@angular/common/http";
import {Http, HttpModule} from '@angular/http';
import { UserGroupService } from './user-group.service';
import {NgbModal, NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {CallbackPipe} from "./callback.pipe";
import { SortPipe } from './app.sort';
import {OrderModule} from 'ngx-order-pipe';
import {Location} from "@angular/common";
import { SurveyComponent } from './survey/survey.component';
import { SurveyService } from './survey.service';
import { SearchComponent } from './search/search.component';
import { NgDatepickerModule } from 'ng2-datepicker';


@NgModule({
  declarations: [
    SortPipe,
    AppComponent,
    UserComponent,
    UserGroupComponent,
    ProjectComponent,
    UserDetailComponent,
    ProjectDetailComponent,
    UserGroupDetailComponent,
    MessagesComponent,
    MainPageComponent,
    CallbackPipe,
    SurveyComponent,
    SearchComponent


  ],
  imports: [
    BrowserModule,
    OrderModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpModule,
    HttpClientModule,
    NgDatepickerModule,
    NgbModule.forRoot()
  ],
  providers: [
    UserService,
    MessageService,
    ProjectService,
    UserGroupService,
    Location,
    NgbModal,
    SurveyService,
    SurveyComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
