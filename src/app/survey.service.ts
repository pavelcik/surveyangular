import { Injectable } from '@angular/core';
import { Project } from './project';
import  { Observable } from "rxjs/Observable";
import { of } from "rxjs/observable/of";
import { MessageService} from "./message.service";
import { HttpClient, HttpHeaders} from "@angular/common/http";
import { catchError, map, tap} from 'rxjs/operators'
import { Survey } from './survey';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable()
export class SurveyService {
  private surveyUrl = 'http://localhost:8080/projects';
  private url;
  constructor(private http: HttpClient,
  private messageService: MessageService) { }
  
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead

      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    this.messageService.add('ProjectService: ' + message);
  }
  
  addSurvey (survey: Survey): Observable<Survey> {
    this.url = this.surveyUrl.concat(survey.id.toString());
    return this.http.post<Survey>(this.url, survey, httpOptions).pipe(
      tap((survey: Survey) => this.log(`added survey w/ id=${[survey.id]}`)),
      catchError(this.handleError<Survey>('addSurvey'))
    );
  }

}
