import { Survey } from '../survey';
import { SurveyService } from '../survey.service';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {FormsModule} from '@angular/forms';
import {NgModule} from '@angular/core';
import {User} from '../user';
import { UserService } from '../user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import "rxjs/add/operator/find";
import {HttpModule} from "@angular/http";

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.css']
})
export class SurveyComponent{
 rForm: FormGroup;
  post:any;
  name:string;

survey: Survey;
user: User;
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private userService: UserService,
    private surveyService: SurveyService,
    private location: Location,
    private forms: FormsModule
  ) {
  this.rForm = fb.group({
  'name' : [null,Validators.required],
    'validate' : ''
  });
  }

  submitted = false;

  onSubmit() { this.submitted = true; }


//  getProject(): void {
//    const id = +this.route.snapshot.paramMap.get('id');
//    this.projectService.getProject(id)
//      .subscribe(project=>this.project=project);
//  }

  // getUserByName(name) {
  //   return this.userService.getUsers().find(x => {
  //     return x.name === name;
  //   });
  // }
  // save(survey) {
  //
  //  survey.user = this.getUserByName(this.name).id;
  //
  //   this.surveyService.addSurvey(this.survey)
  //     .subscribe();
  // }

}
