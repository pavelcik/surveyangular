export class UserGroup{
  id: number;
  groupName: string;
  usersInGroup: number;
}
