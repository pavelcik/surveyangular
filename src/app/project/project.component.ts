import { Component, OnInit, Input } from '@angular/core';
import { Project } from "../project";
import { ProjectService } from "../project.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Location} from "@angular/common";
import {ActivatedRoute,Router} from "@angular/router";


@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  projects: Project[];
  project: Project;
    order: string = 'name';
  reverse: boolean = false;

  constructor(private projectService: ProjectService,
              private route: ActivatedRoute,
              private modalService: NgbModal,
              private location: Location,
            private router: Router) { }

  ngOnInit() {
    this.getProjects();
  }

  getProjects(): void {
    this.projectService.getProjects()
      .subscribe(projects=>this.projects = projects);
  }

  add(project: Project) {
    this.projectService.addProject(this.project)
      .subscribe(project => {
        this.projects.push(project);
      });
  }

  delete(project: Project): void {
    this.projects = this.projects.filter(h => h !== project);
    this.projectService.deleteProject(project).subscribe();
  }

  open(project,modal) {
    this.project = project;
    this.modalService.open(modal);
  }
  openAdd(modal){
    this.modalService.open(modal);
  }

  goBack(): void {
    this.location.back();
  }
  save(): void {
  this.projectService.updateProject(this.project)
.subscribe(() => this.goBack());
}

    setOrder(value: string) {
    if (this.order === value) {
      this.reverse = !this.reverse;
    }

    this.order = value;
  }

  activate(project) {
    this.router.navigateByUrl("projects/addSurvey");

  }


}
