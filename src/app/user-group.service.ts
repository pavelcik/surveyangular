import { Injectable } from '@angular/core';
import {MessageService} from "./message.service";
import {of} from "rxjs/observable/of";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {catchError, tap} from "rxjs/operators";
import {UserGroup} from "./userGroup";
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable()
export class UserGroupService {

  private userGroupUrl = 'http://localhost:8080/userGroups';



  constructor(private http: HttpClient,
              private messageService: MessageService) { }

  getUserGroups(): Observable<UserGroup[]> {
    this.messageService.add('UserGroup list loaded');
    return this.http.get<UserGroup[]>(this.userGroupUrl)
      .pipe(
        tap(userGroups => this.log(`fetched userGroups`)),
        catchError(this.handleError('getUserGroups', []))
      );
  }

  getUserGroup(id: number): Observable<UserGroup> {
    const url = `${this.userGroupUrl}/${id}`;
    return this.http.get<UserGroup>(url).pipe(
      tap(_ => this.log(`fetched userGroup id=${id}`)),
      catchError(this.handleError<UserGroup>(`getHero id=${id}`))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error); // log to console instead

      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  private log(message: string) {
    this.messageService.add('UserGroupService: ' + message);
  }
  updateUserGroup(userGroup: UserGroup): Observable<any> {
    return this.http.put(this.userGroupUrl,userGroup,httpOptions)
      .pipe(tap(_=>this.log('updated userGroup id=${userGroup.id}')),
        catchError(this.handleError<any>('updateUserGroup')));
  }
  addUserGroup (userGroup: UserGroup): Observable<UserGroup> {
    return this.http.post<UserGroup>(this.userGroupUrl, userGroup, httpOptions).pipe(
      tap((userGroup: UserGroup) => this.log(`added userGroup w/ id=${userGroup.id}`)),
      catchError(this.handleError<UserGroup>('addUserGroup'))
    );
  }

  deleteUserGroup (userGroup: UserGroup | number): Observable<UserGroup> {
    const id = typeof userGroup === 'number' ? userGroup : userGroup.id;
    const url = `${this.userGroupUrl}/${id}`;

    return this.http.delete<UserGroup>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted userGroup id=${id}`)),
      catchError(this.handleError<UserGroup>('deleteUserGroup'))
    );
  }
}
